import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class MomentDateService {
    public convertToMoment(date: Date) {
        const value = date && moment(date);
        return value && value.isValid() ? value : null;
    }

    public convertToString(value: Date): string {
        let momentValue = moment(value);
        return momentValue.isValid() ? momentValue.format('YYYY-MM-DD') : '';
    }

    public convertToDate(value: string): Date {
        let momentValue = moment(value);
        return momentValue.isValid() ? momentValue.toDate() : null;
    }
}