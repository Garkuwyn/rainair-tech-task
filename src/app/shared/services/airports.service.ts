import { ISelectListItemModel } from './../models/select-list-item.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from 'app/shared/app-settings';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/shareReplay'

@Injectable()
export class AirportsService {

  constructor(private http: HttpClient) {

  }

  getAirportRoutes(code:string):Observable<ISelectListItemModel[]>{
    return code ? this.getAirportsWithRoutes().map(res => {
      return res.routes[code].map(i=>{
        var airport=res.airports.find(a=>{return a.iataCode===i});
        return <ISelectListItemModel>{
          value:i,
          text:`${airport.name} (${airport.iataCode})`
        };
      });
    }) : this.getAirports();
  }

  getAirports(): Observable<ISelectListItemModel[]> {
    return this.getAirportsWithRoutes().map(res => {
      return res.airports.map(i => {
        return <ISelectListItemModel>{
          text: `${i.name} (${i.iataCode})`,
          value: i.iataCode
        }
      });
    })
  }

  getAirport(code):Observable<any>{
    return this.getAirportsWithRoutes().map(res => {
      return res.airports.find(i => {
        return i.iataCode===code
      });
    })
  }

  private _airportsWithRoutes: Observable<any>;
  getAirportsWithRoutes(): Observable<any> {
    if (!this._airportsWithRoutes)
      return this.http.get(AppSettings.endpoints.airports).shareReplay(1);
    return this._airportsWithRoutes;
  }
}
