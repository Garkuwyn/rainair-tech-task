import { AirportsService } from './services/airports.service';
import { DateWrapperComponent } from './components/date-wrapper/date-wrapper.component';
import { DateSelectorComponent } from './components/date-selector/date-selector.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlightsSearchComponent } from './components/flights-search/flights-search.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders } from '@angular/core';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MomentDateService } from 'app/shared/services/moment-date.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule 
  ],
  declarations: [DateSelectorComponent, DateWrapperComponent, FlightsSearchComponent, AutocompleteComponent],
  exports:[DateSelectorComponent,FlightsSearchComponent, DateWrapperComponent,HttpClientModule, FormsModule,
    ReactiveFormsModule]
})
export class SharedModule { 
  constructor(){

  }

  static forRoot():ModuleWithProviders{
    return {
      ngModule:SharedModule,
      providers:[
        AirportsService,
        MomentDateService
      ]
    }
  }
}
