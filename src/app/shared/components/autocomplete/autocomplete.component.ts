import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ISelectListItemModel } from 'app/shared/models/select-list-item.model';
import { ControlValueAccessor } from '@angular/forms/src/directives/control_value_accessor';
import { ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

export const AUTOCOMPLETE_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => AutocompleteComponent),
  multi: true
};

@Component({
  selector: 'ryanair-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss'],
  providers: [AUTOCOMPLETE_VALUE_ACCESSOR]
})
export class AutocompleteComponent implements OnInit, ControlValueAccessor {
  protected showAutocompleteItems: boolean = false;

  protected filteredItems: ISelectListItemModel[];
  protected inputText: string = '';

  private _currentValue: string;
  private _itemsLimit: number = 100;
  private _items: ISelectListItemModel[];

  constructor(private autoComplete: ElementRef) {}
  public writeValue(val: string): void {
    this._currentValue = val;
    this.propagateChange(val);
  }
  public registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public ngOnInit() { }


  protected textInput(e) {
    this.filter(e.srcElement.value);
    this.inputText = e.srcElement.value;
    let existedItem = this._items.find((i) => i.text === this.inputText);
    if (existedItem) {
      this._currentValue = existedItem.value;
      this.propagateChange(existedItem.value);
    }
    this.showAutocompleteItems = true;
    if (!this.inputText) {
      this._currentValue = '';
      this.propagateChange('');
    }
  }

  protected blur(ev) {
    this.showAutocompleteItems = false;
  }

  protected itemSelected(ev, item: ISelectListItemModel) {
    this._currentValue = item.value;
    this.inputText = ev.srcElement.textContent;
    this.filter(this.inputText);
    this.propagateChange(item.value);
    this.showAutocompleteItems = false;
  }

  protected focus() {
    this.onTouched();
    this.showAutocompleteItems = true;
  }

  protected keyShortcut(ev) {
    if (ev.keyCode === 40 || ev.keyCode === 38) {
      this.upDownBtnsShortcut(ev);
    } else if (ev.keyCode === 13) {
      this.enterBtnShortcut(ev);
    }
  }

  private propagateChange = (_: any) => { };
  private onTouched: any = () => { };

  private filter(val: string) {
    let regex = new RegExp(val.replace(/[\(\)]/g, '\\$&'), 'i');
    let lowerCasedVal = val.toLowerCase();
    this.filteredItems = this._items.filter((option) => option.text.toLowerCase()
      .indexOf(lowerCasedVal) >= 0)
      .map((i) => {
        let match = i.text.match(regex)[0];
        return {
          value: i.value,
          text: `<span>${i.text.replace(match, `<b>${match}</b>`)}</span>`
        }
      })
      .slice(0, this._itemsLimit);
  }

  private upDownBtnsShortcut(ev) {
    let selected = this.autoComplete.nativeElement.querySelector('#focused');
    let  elemToOperate = selected ? ev.keyCode === 40 ?
            selected.nextSibling : selected.previousSibling : null;

    if (elemToOperate) {
      selected.id = '';
      elemToOperate.id = 'focused';
      if (elemToOperate.scrollIntoView){
        elemToOperate.scrollIntoView(false);
      }
    } else {
      let items = this.autoComplete.nativeElement.getElementsByClassName('autocomplete-item');
      let  elemToSelect = items.length ? ev.keyCode === 40 ? 
              items[0] : items[items.length - 1] : null;
      if (elemToSelect){
        elemToSelect.id = 'focused';
      }
    }
  }

  private enterBtnShortcut(ev) {
    let selected = this.autoComplete.nativeElement.querySelector('#focused');
    if (selected) {
      let event = new MouseEvent('mousedown', { bubbles: true });
      selected.dispatchEvent(event);
    }
  }

  @Input() set items(val: ISelectListItemModel[]) {
    if (!val){
      return;
    }
    let item = val.find((i) => i.value === this._currentValue);
    if (item && this._currentValue) {
      this.inputText = item.text;
    } else if (this._currentValue && !item) {
      this.inputText = '';
      this._currentValue = '';
      this.propagateChange('');
    }

    this._items = val.sort((a, b) => { return a.text > b.text ? 1 : -1 });
    this.filter(this.inputText);

  }


}
