import { ControlValueAccessor } from '@angular/forms';
import { Component, EventEmitter, Input, Output, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { MomentDateService } from 'app/shared/services/moment-date.service';

export const DATE_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DateSelectorComponent),
    multi: true
};
@Component({
    selector: 'ryanair-date-selector',
    templateUrl: './date-selector.component.html',
    providers: [DATE_VALUE_ACCESSOR]
})
export class DateSelectorComponent implements ControlValueAccessor {

    protected _date: string;

    constructor(private momentSer: MomentDateService) { }
    public writeValue(value: Date): void {
        this._date = this.momentSer.convertToString(value);
        this.propagateChange(value);
    }
    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }
    public registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }


    protected onInputChanged(value: string) {
        let date = this.momentSer.convertToDate(value);
        this.propagateChange(date);
    }

    protected focus() {
        this.onTouched();
    }

    private propagateChange = (_: any) => { };
    private onTouched: any = () => { };

}
