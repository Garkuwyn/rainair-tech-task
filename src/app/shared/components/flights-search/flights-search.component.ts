import { IFlightsSearchModel } from './../../models/flights-search.model';
import { ISelectListItemModel } from './../../models/select-list-item.model';
import { AirportsService } from './../../services/airports.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MomentDateService } from 'app/shared/services/moment-date.service';


@Component({
  selector: 'ryanair-flights-search',
  templateUrl: './flights-search.component.html',
  styleUrls: ['./flights-search.component.scss']
})
export class FlightsSearchComponent implements OnInit {

  @Input() set model(val: IFlightsSearchModel) {
    if (val){
      this.form.reset({
        outAirport: val.outAirport,
        backAirport: val.backAirport,
        outDate: this.momentSer.convertToDate(val.outDate),
        backDate: this.momentSer.convertToDate(val.backDate)
      });
    }
  };

  protected fromAirports: ISelectListItemModel[];
  protected toAirports: ISelectListItemModel[];

  protected form: FormGroup;

  constructor(
    private _service: AirportsService,
    private fb: FormBuilder,
    private router: Router,
    private momentSer: MomentDateService) {
    this._service.getAirports().subscribe((data) => {
      this.fromAirports = data;
      this.toAirports = data;
    })

    this.form = this.fb.group({
      outAirport: [null, Validators.required],
      backAirport: [null, Validators.required],
      outDate: [null, Validators.required],
      backDate: [null, Validators.required]
    });


    this.form.get('outAirport').valueChanges.subscribe((val) => {
      this._service.getAirportRoutes(val).subscribe((data) => {
        this.toAirports = data;
      });

    });

    this.form.get('backAirport').valueChanges.subscribe((val) => {
      this._service.getAirportRoutes(val).subscribe((data) => {
        this.fromAirports = data;
      });
    });

    this.form.get('outDate').valueChanges.subscribe((value) => {
      const startDate = this.momentSer.convertToMoment(value);
      const endDate = this.momentSer.convertToMoment(this.form.get('backDate').value);

      if (startDate && endDate && startDate > endDate) {
        this.form.get('backDate').setValue(startDate.add(2, 'd').toDate());
      }
    });

    this.form.get('backDate').valueChanges.subscribe((value) => {
      const startDate = this.momentSer.convertToMoment(this.form.get('outDate').value);
      const endDate = this.momentSer.convertToMoment(value);
      if (endDate && startDate && startDate > endDate) {
        this.form.get('outDate').setValue(endDate.subtract(2, 'd').toDate());
      }
    });

  }

  public ngOnInit() {
  }

  public search() {
    let from = this.form.get('outAirport').value;
    let  to = this.form.get('backAirport').value;
    let outDate = this.momentSer.convertToString(this.form.get('outDate').value);
    let  backDate = this.momentSer.convertToString(this.form.get('backDate').value);

    this.router.navigate(['home/cheap-flights', from, to, outDate, backDate]);
  }

  public isInvalid(controlName: string) {
    return this.form.get(controlName).touched
      && this.form.get(controlName).invalid;
  }

}