import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MomentDateService } from 'app/shared/services/moment-date.service';

@Component({
    selector: 'ryanair-date-wrapper',
    templateUrl: './date-wrapper.component.html'
})
export class DateWrapperComponent {
    @Output() startDateChanged: EventEmitter<Date> = new EventEmitter();
    @Output() endDateChnaged: EventEmitter<Date> = new EventEmitter();
    @Input() startDate: Date = null;
    @Input() endDate: Date = null;

    constructor(private momentDateSer:MomentDateService) {
    }

    onStartDateChange(value: Date) {
        this.startDate = value;
        this.startDateChanged.emit(value);
        const startDate = this.momentDateSer.convertToMoment(value);
        const endDate = this.momentDateSer.convertToMoment(this.endDate);
        if (startDate && endDate && startDate > endDate) {
            this.endDate = startDate.add(2, 'd').toDate();
            this.endDateChnaged.emit(this.endDate);
        }
    }

    onEndDateChange(value: Date) {
        this.endDate = value;
        this.endDateChnaged.emit(value);
        const startDate = this.momentDateSer.convertToMoment(this.startDate);
        const endDate = this.momentDateSer.convertToMoment(value);
        if (endDate && startDate && startDate > endDate) {
            this.startDate = endDate.subtract(2, 'd').toDate();
            this.startDateChanged.emit(this.startDate);
        }
    }
}
