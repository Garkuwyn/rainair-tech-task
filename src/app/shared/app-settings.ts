export class AppSettings{
    static endpoints={
        airports:'https://murmuring-ocean-10826.herokuapp.com/en/api/2/forms/flight-booking-selector/',
        flights:'https://murmuring-ocean-10826.herokuapp.com/en/api/2/flights/from/{from}/to/{to}/{fromDate}/{toDate}/250/unique/?limit=15&offset-0'
    }

    public static formatUrl = (url: string, ...args: any[]): string => {
        let regex = /\{{1}\w*\}{1}/g,
          matches = url.match(regex);
        matches.forEach((m, index) => {
          url = url.replace(m, args[index]);
        })
        return url;
      }
}