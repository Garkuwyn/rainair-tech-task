export interface IFlightsSearchModel{
    outDate:string;
    backDate:string;
    outAirport:string;
    backAirport:string;
}