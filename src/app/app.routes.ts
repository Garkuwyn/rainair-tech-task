import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { FlightsComponent } from 'app/flights/flights.component';

export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'home', children: [
      { path: '', component: HomeComponent },
      { path: 'cheap-flights/:outAirport/:backAirport/:outDate/:backDate', component: FlightsComponent }
    ]
  }
];
