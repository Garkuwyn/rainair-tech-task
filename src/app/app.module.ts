import { SharedModule } from './shared/shared.module';
import { DateSelectorComponent } from './dateComponents/date-selector/date-selector.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/*
 * Platform and Environment providers/directives/pipes
 */
import { environment } from 'environments/environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { HomeComponent } from './home';

import '../styles/styles.scss';
import { DateWrapperComponent } from 'app/shared/date-wrapper/date-wrapper.component';
import { FlightsComponent } from './flights/flights.component';
import { FlightsService } from 'app/flights/flights.service';

// Application wide providers
// const APP_PROVIDERS = [
//   ...APP_RESOLVER_PROVIDERS
// ];

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    HomeComponent,
    FlightsComponent
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false,
      preloadingStrategy: PreloadAllModules
    }),
    SharedModule.forRoot()
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    environment.ENV_PROVIDERS,
   // APP_PROVIDERS,
    FlightsService
  ]
})
export class AppModule {}
