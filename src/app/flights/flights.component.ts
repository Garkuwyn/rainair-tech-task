import { IFlightsSearchModel } from 'app/shared/models/flights-search.model';
import { FlightsService } from './flights.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IFlightModel } from 'app/flights/flight.model';
import { AirportsService } from 'app/shared/services/airports.service';
import * as moment from 'moment';

@Component({
  selector: 'ryanair-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss']
})
export class FlightsComponent implements OnInit {

  public flightsData: IFlightModel[] = [];
  public searchModel: IFlightsSearchModel;

  public airportFromName: string;
  public airportToName: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private flightsSer: FlightsService,
    private airportsSer: AirportsService) {

    this.activatedRoute.params.subscribe((params) => {
      let model = <IFlightsSearchModel>{
        outAirport: params['outAirport'],
        backAirport: params['backAirport'],
        outDate: params['outDate'],
        backDate: params['backDate']
      };
      this.searchModel = model;
      this.flightsSer.searchFlights(model).subscribe((data) => {
        this.flightsData = data;
      });
      this.airportsSer.getAirport(model.outAirport).subscribe((i) => {
        this.airportFromName = i.name;
      });
      this.airportsSer.getAirport(model.backAirport).subscribe((i) => {
        this.airportToName = i.name;
      });
    });
  }

  public ngOnInit() {
  }

  public getDisplayDate(date) {
    let momentValue = moment(date);
    return momentValue && momentValue.isValid() ? momentValue.format('MMMM Do YYYY, h:mm a') : null;
  }

}
