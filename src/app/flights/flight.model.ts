export interface IFlightModel{
    dateFrom: Date;
    dateTo: Date;
    currency: string;
    price: string;
}