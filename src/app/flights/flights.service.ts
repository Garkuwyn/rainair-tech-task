import { IFlightsSearchModel } from 'app/shared/models/flights-search.model';
import { AppSettings } from 'app/shared/app-settings';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IFlightModel } from 'app/flights/flight.model';

@Injectable()
export class FlightsService {

  constructor(private http:HttpClient) {

   }

  public searchFlights(model: IFlightsSearchModel): Observable<IFlightModel[]>{
    let url=AppSettings.formatUrl(AppSettings.endpoints.flights,model.outAirport,
            model.backAirport,model.outDate, model.backDate);
    return this.http.get(url).map((res) => {
      return <IFlightModel[]>res['flights'];
    });
  }

}
