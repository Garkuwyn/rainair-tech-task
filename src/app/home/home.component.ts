import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'home',  // <home></home>
 
  styleUrls: [ './home.component.css' ],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './home.component.html'
})
export class HomeComponent {

}
